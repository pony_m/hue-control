# HueControl

Control Lights at home using mqtt

## Discovery and config of Bridge

- Get the IP of the bridge
- visit `https://<bridge ip address>/debug/clip.html`
- Run `huec-control create-user <username>` to retrieve a username for the bridge
- Add `username` to `hue2mqtt.toml`
- Start broker and hue2mqtt services `docker-compose up -d`

## Listen to events

    sudo apt install -y mosquitto-clients
    mosquitto_sub -h 127.0.0.1 -V mqttv31 --pretty -d -t hue2mqtt/#

## Audio Streaming

Install portaudio deps

    sudo apt install libportaudio2
