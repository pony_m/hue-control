import asyncio
import os
import sys
import aiohttp
import click
import gmqtt
from hue2mqtt.hue2mqtt import Hue2MQTT

# Backwards compatibility for TOML in stdlib from Python 3.11
try:
    import tomllib  # type: ignore[import,unused-ignore]
except ModuleNotFoundError:
    import tomli as tomllib  # type: ignore[import,no-redef,unused-ignore]


@click.group()
def cli():
    pass


def get_debug_page(config: Hue2MQTT):
    return f"https://{config.config.hue.ip}/debug/clip.html"


def api_url(config):
    return f"http://{config.config.hue.ip}/api"


JsonResp = tuple[list | dict, int]


def sync_request(url: str, method: str = "get", **request_kwargs) -> JsonResp:
    async def make_request() -> JsonResp:
        async with aiohttp.ClientSession() as session:
            async with getattr(session, method)(url, **request_kwargs) as r:
                return await r.json(), r.status

    loop = asyncio.get_event_loop()
    r = loop.run_until_complete(make_request())
    return r


@cli.command()
@click.argument("username")
@click.option("-c", "--config-file", type=click.Path(exists=True))
def create_user(username: str, config_file):
    cfg = Hue2MQTT(False, config_file)
    r, status = sync_request(
        url=api_url(cfg), method="post", json={"devicetype": username}
    )
    for resp in r:
        err = resp.get("error")
        if err:
            print(resp.get("description"))
            sys.exit(1)
        succ = resp.get("success")
        if not succ:
            continue
        print(succ["username"])


@cli.command()
@click.option("-c", "--config-file", type=click.Path(exists=True))
def debug_page(config_file):
    cfg = Hue2MQTT(False, config_file)
    print(f"Visit '{get_debug_page(cfg)}'")


@cli.command()
@click.option("-h", "--host", default="127.0.0.1")
@click.option('--port', default=1883, help='MQTT broker port')
@click.option("-t", "--topic", multiple=True, default=("hue2mqtt/#",))
def listen(host, port, topic):
    """Listen to hue2mqtt messages"""
    import asyncio
    import signal

    from gmqtt import Client as MQTTClient

    # gmqtt also compatibility with uvloop
    import uvloop
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

    STOP = asyncio.Event()

    def on_connect(client, flags, rc, properties):
        print('Connected')
        # client.subscribe(lights_topic, qos=0)
        # client.subscribe(groups_topic, qos=0)
        for to in topic:
            client.subscribe(to)

    def on_message(client, topic, payload, qos, properties):
        print('RECV MSG:', payload)

    def on_disconnect(client, packet, exc=None):
        print('Disconnected')

    def on_subscribe(client, mid, qos, properties):
        print('SUBSCRIBED')

    def ask_exit(*args):
        STOP.set()

    async def main(broker_host, token):
        client = MQTTClient(None)

        client.on_connect = on_connect
        client.on_message = on_message
        client.on_disconnect = on_disconnect
        client.on_subscribe = on_subscribe

        # client.set_auth_credentials(token, None)
        await client.connect(broker_host)

        # client.publish('TEST/TIME', str(time.time()), qos=1)

        await STOP.wait()
        await client.disconnect()

    loop = asyncio.get_event_loop()
    loop.add_signal_handler(signal.SIGINT, ask_exit)
    loop.add_signal_handler(signal.SIGTERM, ask_exit)

    loop.run_until_complete(main(host, None))
