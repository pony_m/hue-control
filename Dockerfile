# Creating a python base with shared environment variables
# The production build will use this directly as base, copyiing only the venv from
# the production-build stage
ARG PYTHON_VERSION=3.11
FROM python:$PYTHON_VERSION-slim as python-base
ARG DEV_UID="1000"

# The env vars are inherited in all images that use FROM python-base as ....
# OSTYPE has be to set since it is normally there in linux and used by /venv/bin/activate
ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VIRTUALENVS_IN_PROJECT=false \
    POETRY_NO_INTERACTION=1 \
    OSTYPE='linux-gnu'
# Setting up proper permissions, running app not as root
# install dependencies for your application, update system
RUN mkdir -p /app \
    && groupadd --gid $DEV_UID -r dev \
    && useradd --create-home -d /home/dev --uid $DEV_UID -r -g dev dev \
    && chown dev:dev -R /app \
    && apt-get update && apt-get upgrade -y

ENV PATH="/home/dev/.local/bin:$PATH"

FROM python-base as development

USER dev
RUN ls -la /home/dev
RUN pip install -U poetry

# We copy our Python requirements here to cache them and install only runtime deps using poetry
WORKDIR /app
COPY --chown=dev:dev ./poetry.lock ./pyproject.toml ./

# 'development' stage installs all dev deps and can be used to develop code.
# For example using docker-compose to mount local volume under /app
USER dev
RUN poetry install
COPY --chown=dev:dev . .

# small hack to get cli installed, which needs the source code
RUN poetry install --only-root

CMD ["poetry", "run", "hue-control", "--help"]

FROM development as prod-builder
# Since we deal with a cli that can not be installed during poetry install (none of the code is present at this step)
RUN poetry build -f sdist && ls ./dist

FROM python-base as production
WORKDIR /app
COPY --chown=dev:dev --from=prod-builder /app/dist ./dist
USER dev
RUN pip install $(find dist -type f -name "*.tar.gz") && rm -r dist

CMD ["hue2mqtt", "--help"]
